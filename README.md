# OCR Calculator ML Kit

A simple calculator app that can calculate two variables using ML Kit Text Recognition

## Library used in this app

    def camerax_version = "1.2.2"
    implementation "androidx.camera:camera-core:${camerax_version}"
    implementation "androidx.camera:camera-camera2:${camerax_version}"
    implementation "androidx.camera:camera-lifecycle:${camerax_version}"

    implementation 'com.google.mlkit:text-recognition:16.0.0'

