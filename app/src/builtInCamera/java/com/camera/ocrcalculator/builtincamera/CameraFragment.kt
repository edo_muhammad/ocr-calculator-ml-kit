package com.camera.ocrcalculator.builtincamera

import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.CameraSelector
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCapture.OnImageCapturedCallback
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.ImageProxy
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.camera.ocrcalculator.model.arg.LandingResultArgs
import com.core.ocrcalculator.OCRCalculatorException
import com.core.ocrcalculator.OCRCalculatorResultCallback
import com.camera.ocrcalculator.databinding.ActivityCameraBinding
import com.core.ocrcalculator.OCRCalculator
import com.core.ocrcalculator.model.ResultData
import com.google.mlkit.vision.common.InputImage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CameraFragment : Fragment() {

	private var _binding: ActivityCameraBinding? = null
	private val binding get() = _binding!!
	private val ocrCalculator: OCRCalculator = OCRCalculator()

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View {
		_binding = ActivityCameraBinding.inflate(inflater, container, false)

		return binding.root
	}

	@ExperimentalGetImage
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		initCamera()

		binding.btnTakePicture.setOnClickListener {
			takePicture()
		}
	}

	@ExperimentalGetImage
	private fun takePicture() {
		binding.btnTakePicture.isClickable = false
		val imageCapture = imageCapture ?: return

		this.context?.let {
			imageCapture.takePicture(
				ContextCompat.getMainExecutor(it),

				object : OnImageCapturedCallback() {

					override fun onCaptureSuccess(imageProxy: ImageProxy) {
						super.onCaptureSuccess(imageProxy)
						val mediaImage = imageProxy.image

						mediaImage?.let {
							val image =
								InputImage.fromMediaImage(it, imageProxy.imageInfo.rotationDegrees)

							imageProxy.close()
							ocrCalculator.processInputImage(
								image,
								object : OCRCalculatorResultCallback {
									override fun onFinishCalculation(resultData: ResultData) {
										navigateToLandingWithResult(image.bitmapInternal, resultData)
									}

									override fun onFailedCalculation(e: OCRCalculatorException) {
										e.printStackTrace()

										Toast.makeText(
											this@CameraFragment.context,
											"Can't calculate, invalid number given!",
											Toast.LENGTH_LONG
										).show()

										Handler(Looper.getMainLooper()).postDelayed({
											navigateToLandingWithResult()
										}, 400)
									}
								}
							)
						}
					}

					override fun onError(exception: ImageCaptureException) {
						super.onError(exception)

						exception.printStackTrace()

						binding.btnTakePicture.isClickable = true
					}
				}
			)
		}
	}

	private fun navigateToLandingWithResult(bitmap: Bitmap? = null, resultData: ResultData? = null) {
		findNavController().navigate(
			CameraFragmentDirections.popUpToLanding(
				if (resultData == null || bitmap == null) null else LandingResultArgs(
					bitmap,
					resultData
				)
			)
		)
	}

	var imageCapture: ImageCapture? = null

	private fun initCamera() {
		context?.let {
			val cameraProviderFuture = ProcessCameraProvider.getInstance(it)

			imageCapture = ImageCapture.Builder().build()

			cameraProviderFuture.addListener({
				val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

				val preview = Preview.Builder()
					.build()
					.also {
						it.setSurfaceProvider(binding.viewFinder.surfaceProvider)
					}

				val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

				try {
					cameraProvider.unbindAll()

					cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)
				} catch (exc: Exception) {
					exc.printStackTrace()
				}
			}, ContextCompat.getMainExecutor(it))
		}
	}
}