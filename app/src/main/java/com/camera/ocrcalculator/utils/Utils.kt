package com.camera.ocrcalculator.utils

import android.graphics.Bitmap
import android.graphics.Matrix
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.exifinterface.media.ExifInterface
import androidx.fragment.app.Fragment
import java.io.File

fun fixImageBitmapRotation(fragment: Fragment, uri: Uri): Pair<Bitmap, Int> {
	val fileUtils = FileUtils(fragment.context)
	val sourceBitmap = MediaStore.Images.Media.getBitmap(fragment.activity?.contentResolver, uri)
	val exif = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
		ExifInterface(File(fileUtils.getPath(uri)!!))
	} else {
		ExifInterface(uri.path.orEmpty())
	}

	val rotation = exif.getAttributeInt(
		ExifInterface.TAG_ORIENTATION,
		ExifInterface.ORIENTATION_NORMAL
	)
	val rotationInDegrees = exifToDegrees(rotation)
	val matrix = Matrix()

	if (rotation != 0) {
		matrix.preRotate(rotationInDegrees.toFloat())
	}

	return Pair(
		Bitmap.createBitmap(
			sourceBitmap,
			0,
			0,
			sourceBitmap.width,
			sourceBitmap.height,
			matrix,
			true
		),
		rotationInDegrees
	)
}

fun exifToDegrees(exifOrientation: Int): Int {
	if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
		return 90
	} else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
		return 180
	} else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
		return 270
	}
	return 0
}