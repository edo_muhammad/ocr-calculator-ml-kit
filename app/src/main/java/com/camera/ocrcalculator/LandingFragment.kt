package com.camera.ocrcalculator

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.camera.ocrcalculator.databinding.FragmentLandingBinding

class LandingFragment : Fragment() {

	private var _binding: FragmentLandingBinding? = null
	private val binding get() = _binding!!

	val args by navArgs<LandingFragmentArgs>()

	private val requestPermissionLauncher =
		registerForActivityResult(
			ActivityResultContracts.RequestPermission()
		) { isGranted: Boolean ->
			if (isGranted) {
				navigateToCamera()
			} else {
				Toast.makeText(context, "Permission denied!", Toast.LENGTH_SHORT).show()
			}
		}

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View {
		_binding = FragmentLandingBinding.inflate(inflater, container, false)

		return binding.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		binding.btnInput.setOnClickListener {
			if(BuildConfig.FLAVOR_feature == FLAVOUR_BUILT_IN_CAMERA) {
				checkRequiredPermission()
			} else {
				navigateToPhotoPicker()
			}
		}

		args.landingArgs?.let {
			val resultData = it.resultData

			binding.txtInput.text = getString(R.string.title_input).replace("-", resultData.rawData)
			binding.txtResult.text = getString(R.string.title_result).replace("-", resultData.result)

			binding.imgResult.setImageBitmap(it.bitmap)
		}
	}

	private fun checkRequiredPermission() {
		activity?.let {
			if(ContextCompat.checkSelfPermission(it, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
				navigateToCamera()
			} else  {
				requestPermissionLauncher.launch(Manifest.permission.CAMERA)
			}
		}
	}

	private fun navigateToCamera() {
		findNavController().navigate(LandingFragmentDirections.openCameraFragment())
	}

	private fun navigateToPhotoPicker() {
		findNavController().navigate(LandingFragmentDirections.openPhotoPickerFragment())
	}

	companion object {
		const val FLAVOUR_BUILT_IN_CAMERA = "builtInCamera"
		const val FLAVOUR_FILE_SYSTEM = "fileSystem"
	}
}