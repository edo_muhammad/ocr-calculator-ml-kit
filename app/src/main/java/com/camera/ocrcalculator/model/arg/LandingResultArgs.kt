package com.camera.ocrcalculator.model.arg

import android.graphics.Bitmap
import android.os.Parcelable
import com.core.ocrcalculator.model.ResultData
import kotlinx.parcelize.Parcelize

@Parcelize
data class LandingResultArgs(
	var bitmap: Bitmap,
	var resultData: ResultData
): Parcelable