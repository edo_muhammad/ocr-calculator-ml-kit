package com.camera.ocrcalculator

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class CalculatorUnitTest {
	@Test
	fun `given variables to calculate then should calculate properly`() {
		val trimString = "10 * 10".replace(" ", "")
		var firstDigitString = ""
		var secondDigitString = ""
		var operator = ""
		var result = 0

		for (i in trimString.indices) {
			if(trimString[i].isDigit()) {
				if(operator.isEmpty()) {
					firstDigitString += trimString[i]
				} else {
					secondDigitString += trimString[i]
				}
			} else {
				if(trimString.contains(Regex("[+/*-]")) && operator.isEmpty() && firstDigitString.isNotEmpty()) {
					operator = trimString[i].toString()
				} else {
					break
				}
			}
		}

		if(operator.equals("+")) {
			result = firstDigitString.toInt() + secondDigitString.toInt()
		} else if(operator.equals("-")) {
			result = firstDigitString.toInt() - secondDigitString.toInt()
		} else if(operator.equals("*")) {
			result = firstDigitString.toInt() * secondDigitString.toInt()
		} else {
			result = firstDigitString.toInt() / secondDigitString.toInt()
		}

		assertEquals("10", firstDigitString)
		assertEquals("10", secondDigitString)
		assertEquals(100, result)
	}
}