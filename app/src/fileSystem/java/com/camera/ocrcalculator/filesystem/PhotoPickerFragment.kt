package com.camera.ocrcalculator.filesystem

import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.camera.ocrcalculator.databinding.FragmentPhotoPickerBinding
import com.camera.ocrcalculator.model.arg.LandingResultArgs
import com.camera.ocrcalculator.utils.fixImageBitmapRotation
import com.core.ocrcalculator.OCRCalculator
import com.core.ocrcalculator.OCRCalculatorException
import com.core.ocrcalculator.OCRCalculatorResultCallback
import com.core.ocrcalculator.model.ResultData
import com.google.mlkit.vision.common.InputImage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PhotoPickerFragment : Fragment() {

	private var _binding: FragmentPhotoPickerBinding? = null
	private val binding get() = _binding!!

	private val ocrCalculator: OCRCalculator = OCRCalculator()

	private val pickMedia =
		registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
			if (uri != null) {
				Log.d("PhotoPicker", "Selected URI: $uri")

				onUserSelectImage(uri)
			} else {
				Log.d("PhotoPicker", "No media selected")

				onUserCancelSelectImage()
			}
		}

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View {
		_binding = FragmentPhotoPickerBinding.inflate(inflater, container, false)

		return binding.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		pickMedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
	}

	private fun onUserSelectImage(uri: Uri) {
		try {
			val fixBitmapPair = fixImageBitmapRotation(this, uri)

			initOcrCalculator(fixBitmapPair)
		} catch (e: Exception) {
			e.printStackTrace()
		}
	}

	private fun onUserCancelSelectImage() {
		Toast.makeText(
			this@PhotoPickerFragment.context,
			"No picture selected!",
			Toast.LENGTH_LONG
		).show()

		navigateToLandingWithResult()
	}

	private fun initOcrCalculator(fixBitmapPair: Pair<Bitmap, Int>) {
		ocrCalculator.processInputImage(
			InputImage.fromBitmap(fixBitmapPair.first, fixBitmapPair.second),
			object : OCRCalculatorResultCallback {
				override fun onFinishCalculation(resultData: ResultData) {
					navigateToLandingWithResult(fixBitmapPair.first, resultData)
				}

				override fun onFailedCalculation(e: OCRCalculatorException) {
					e.printStackTrace()

					lifecycleScope.launch(Dispatchers.Main) {
						Toast.makeText(
							this@PhotoPickerFragment.context,
							"Can't calculate, invalid photo given!",
							Toast.LENGTH_LONG
						).show()
					}

					navigateToLandingWithResult()
				}
			}
		)
	}

	private fun navigateToLandingWithResult(
		bitmap: Bitmap? = null,
		resultData: ResultData? = null
	) {
		Handler(Looper.getMainLooper()).postDelayed({
			findNavController().navigate(
				PhotoPickerFragmentDirections.popUpToLanding(
					if (resultData == null || bitmap == null) null else LandingResultArgs(
						bitmap,
						resultData
					)
				)
			)
		}, 400)
	}
}