package com.core.ocrcalculator.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ResultData(
	var variables: MutableList<String>,
	var operator: String,
	var result: String,
	var rawData: String
) : Parcelable