package com.core.ocrcalculator

import com.core.ocrcalculator.model.ResultData
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.TextRecognizer
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import java.lang.NumberFormatException

class OCRCalculator(textRecognizerOptions: TextRecognizerOptions = TextRecognizerOptions.DEFAULT_OPTIONS) {

	private val textRecognizer: TextRecognizer = TextRecognition.getClient(textRecognizerOptions)

	fun processInputImage(
		inputImage: InputImage,
		ocrCalculatorResultCallback: OCRCalculatorResultCallback
	) {
		textRecognizer.process(inputImage).addOnSuccessListener {
			val result = calculate(it.text)

			result.second?.let {
				ocrCalculatorResultCallback.onFailedCalculation(it)
			} ?: kotlin.run {
				result.first?.let {
					ocrCalculatorResultCallback.onFinishCalculation(it)
				}
			}
		}.addOnFailureListener {
			ocrCalculatorResultCallback.onFailedCalculation(OCRCalculatorException(it.message.orEmpty()))
		}
	}

	private fun calculate(givenRawString: String): Pair<ResultData?, OCRCalculatorException?> {
		val trimString = givenRawString.replace(" ", "")
		var firstDigitString = ""
		var secondDigitString = ""
		var operator = ""

		for (i in trimString.indices) {
			if (trimString[i].isDigit()) {
				if (operator.isEmpty()) {
					firstDigitString += trimString[i]
				} else {
					secondDigitString += trimString[i]
				}
			} else {
				if (trimString.contains(Regex("[+/*-]")) && operator.isEmpty() && firstDigitString.isNotEmpty()) {
					operator = trimString[i].toString()
				} else {
					if (firstDigitString.isEmpty()) {
						Pair(null, OCRCalculatorException("Invalid digit given! ($trimString"))
					} else if (firstDigitString.isNotEmpty() && operator.isEmpty()) {
						Pair(
							null,
							OCRCalculatorException("Invalid operator given!, operator should be [+ - / *]")
						)
					}
				}
			}
		}

		try {
			val resultCalculation =
				calculateDigit(firstDigitString.toInt(), secondDigitString.toInt(), operator)

			return Pair(
				ResultData(
					variables = mutableListOf(firstDigitString, secondDigitString),
					operator = operator,
					result = resultCalculation.toString(),
					rawData = givenRawString
				), null
			)
		} catch (e: NumberFormatException) {
			e.printStackTrace()

			return Pair(null, OCRCalculatorException(e.message.orEmpty()))
		}
	}

	private fun calculateDigit(
		firstDigit: Int,
		secondDigit: Int,
		operator: String
	): Int = when (operator) {
		"+" -> {
			firstDigit + secondDigit
		}

		"-" -> {
			firstDigit - secondDigit
		}

		"*" -> {
			firstDigit * secondDigit
		}

		else -> firstDigit / secondDigit
	}
}