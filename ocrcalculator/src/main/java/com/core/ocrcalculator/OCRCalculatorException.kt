package com.core.ocrcalculator

class OCRCalculatorException(errorMessage: String): Exception(errorMessage)