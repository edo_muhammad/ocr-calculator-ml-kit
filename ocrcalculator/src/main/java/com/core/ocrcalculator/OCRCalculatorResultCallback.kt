package com.core.ocrcalculator

import com.core.ocrcalculator.model.ResultData

interface OCRCalculatorResultCallback {
	fun onFinishCalculation(resultData: ResultData)
	fun onFailedCalculation(e: OCRCalculatorException)
}